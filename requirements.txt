tensorflow==2.12.1; sys_platform == 'linux'
tensorflow==2.12.1; sys_platform == 'win32'
tensorflow-macos==2.12.1; sys_platform == 'darwin'
optuna==3.2.0
ray[air]==2.5.1
grpcio==1.49.1
pydantic<2.0.0
numpy
scipy
pandas
matplotlib
seaborn
tabulate
scikit-learn
pytest
pre-commit<=3.3.2
multiprocess; sys_platform == 'darwin'