import os
import numpy as np

# OPTIMA environment setup
path_to_setup_file = os.path.join("setup_OPTIMA_cpu.sh")  # sourceable file that sets up the anaconda environment

inputs_file = os.path.join("resources", "events_PolOnly_LL.pickle")  # file containing the input data (e.g. output from extract_data_from_Trees.py)
use_testing_dataset = False  # should the dataset be split into training / validation or training / validation / testing? (crossvalidation will also be performed with the same number of splits)
fixed_testing_dataset = False  # should the same testing dataset be used for all folds during crossvalidation or shuffled like the validation dataset? Only used if explicit_testing_dataset == True
use_eventNums_splitting = True  # should inputs be split randomly into training, validation (and test) or should the event numbers be used?
max_num_events = 10000  # limit the maximum number of events to use; can be np.inf to use all available input data

# output folder
output_name = "example"  # suffix appended to output folder name (which by default also contains the algorithm used and the validation offset as C# when using event numbers for splitting)
use_exact_name = True  # omit the automatically created part of the output folder name?

# which input variables to give to the DNN. When using variable optimization, these are the input variables for the initial optimization phase.
# For the remaining optimization, the optimized set of input variables will be used.
input_vars = ['jet1_pt', 'jet1_phi_recalibrated', 'jet1_eta',
              'jet2_pt', 'jet2_phi_recalibrated', 'jet2_eta',
              'lep1_pt', 'lep1_eta',
              'lep2_pt', 'lep2_phi_recalibrated', 'lep2_eta',
              'met_pt', 'met_phi_recalibrated']

# input scaling for each input variable
input_scaling = {
    "jet1_pt": lambda x: np.log10(x),
    "jet1_phi_recalibrated": lambda x: x,
    "jet1_eta": lambda x: x,
    "jet2_pt": lambda x: np.log10(x),
    "jet2_phi_recalibrated": lambda x: x,
    "jet2_eta": lambda x: x,
    "lep1_pt": lambda x: np.log10(x),
    "lep1_eta": lambda x: x,
    "lep2_pt": lambda x: np.log10(x),
    "lep2_phi_recalibrated": lambda x: x,
    "lep2_eta": lambda x: x,
    "met_pt": lambda x: np.log10(x),
    "met_phi_recalibrated": lambda x: x,
}

# general settings for the optimization
max_epochs = 20  # maximum number of epoch before terminating the training; this can be something very large because the Early Stopping and ASHA will take care of the termination
early_stopping_patience = 6  # number of consecutive epochs without improvement (of the metric to monitor) before terminating the training

# settings for the evaluation
evaluation_class_labels = ["LL", "Bkg."]  # list of labels used as the names of the different classes

# settings for the initial hyperparameter optimization and subsequent input variable optimization
perform_variable_opt = True  # do the initial hyperparameter optimization step with Optuna and subsequent input variable optimization
num_samples_variableOpt = 20  # how many hyperparameter combinations to try before doing the variable optimization

# settings of the main hyperparameter optimization
perform_main_hyperopt = True  # do the main hyperparameter optimization step with Optuna
num_samples_main = 100  # how many hyperparameter combinations to try

# settings of the hyperparameter fine-tuning step using Population Based Training
perform_PBT_hyperopt = True  # do the hyperparameter fine-tuning step with PBT
num_samples_PBT = 10  # how large should the population be?

# settings for the hyperparameter optimization with Optuna and the Asynchronous HyperBand scheduler (ASHA); this affects both the main and the pre-optimization steps
ASHA_grace_period = 15  # grace period before ASHA starts terminating trails
ASHA_max_t = max_epochs  # end point of the reduction

# settings for the hyperparameter optimization with Population Based Training
perturbation_interval = 6  # number of epochs to train between perturbations
burn_in_period = 6  # number of epochs to wait before first perturbation

# settings for the input variable optimization
variable_optimization_mode = 'shuffle'  # which algorithm to use to evaluate variable importance. Options are 'retrain' (for retraining new models with different input variables), 'shuffle' (for shuffling dropped input variable with fixed models), 'hybrid' (combines 'shuffle' and 'retrain') and 'custom' (for providing an own method)
num_repeats = 10  # how often should each variable set be evaluated per iteration
num_repeats_for_reevaluation = 3  # the number of times the re-evaluation of the best variable set should be done
var_opt_patience = 5  # early-stopping like patience to stop the variable optimization when no improvement for several iterations; is not used if mode is 'shuffle' and retrain_for_reevaluation is False or if choose_best_var is False

# hyperparameter search space. The range of hyperparameters can be specified by:
#   - giving a fixed value --> no optimization
#   - giving a list --> only specified values will be tried
#   - giving a tuple (keyword, a, b, step), where keyword can be 'uniform' or 'log' (in which case the values will be
#     drawn from a uniform/logarithmic distribution between a and b) or 'normal' (in which case the values will be drawn
#     from a gaussian distribution with mean a and standard deviation b). step is optional and if given, the distribution
#     will be discretized with step size step. For 'uniform' and 'log', the datatype of a will decide the datatype of the
#     drawn value
# For the built-in MLP: tunable hyperparameters are: "num_layers", "units", "units_i" where i is the ith layer (WARNING:
# when specifying units for each layer, make sure to give at least as many entries as the maximum possible number of layers
# and set "optimize_units_per_layer" to True!), "activation", "kernel_initializer", "bias_initializer",
# "dropout", "batch_size", "learning_rate", "Adam_beta_1", "one_minus_Adam_beta_2", "Adam_epsilon" and "loss_weight_class_i"
# (with i between 0 and the number of classes - 1; WARNING: you need to specify either no or as many loss_weight_class
# entries as there are classes). For all hyperparameters (besides loss_weight_class_i) that are not specified here,
# default values given in OptimizationTools.get_hp_defaults() will be used.
search_space = {
    "num_layers": ('uniform', 6, 12, 1),
    "units": ('log', 32, 128, 1),
    "activation": 'swish',
    "kernel_initializer": 'auto',
    "bias_initializer": 'auto',
    "l1_lambda": 0.,  # ('log', 1e-18, 1e-1),
    "l2_lambda": 0.,  # ('log', 1e-18, 1e-1),
    "dropout": ('uniform', 0.0, 0.2),
    "batch_size": ('uniform', 64., 512.),
    "learning_rate": ('log', 1e-5, 1e-2),
    "Adam_beta_1": ('uniform', 1e-4, 0.99),
    "one_minus_Adam_beta_2": ('log', 1e-5, 0.9999),
    "Adam_epsilon": ('log', 1e-10, 1.0),
    "loss_function": 'BinaryCrossentropy',
}