#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Convenience wrapper for running the manager_ray_nodes helper directly from source tree."""

from OPTIMA.helpers.manage_ray_nodes import main

if __name__ == "__main__":
    main()
