# -*- coding: utf-8 -*-

"""OPTIMA.__main__: executed when OPTIMA directory is called as script."""

from .optima import main
main()