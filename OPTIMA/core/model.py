# -*- coding: utf-8 -*-
"""A module that provides abstract functionality to interact with models from different libraries."""
from types import ModuleType
from typing import Any, Optional, Union

import numpy as np

import tensorflow as tf


model_config_type = dict[str, Union[int, float, str, Any]]


class AbstractModel:
    """Simple wrapper to provide generic model functionality for different machine learning libraries.

    Currently, only Keras models are supported.
    """

    def __init__(self, run_config: ModuleType, model: Any) -> None:
        """Constructr of AbstractModel.

        Parameters
        ----------
        run_config : ModuleType
            Reference to the imported `run-config`-file.
        model : Any
            The model that should be wrapped.
        """
        self.run_config = run_config
        self.model = model

    def predict(self, inputs: np.ndarray, verbose: int = 0) -> np.ndarray:
        """Calculates the model prediction for the provided inputs array.

        Parameters
        ----------
        inputs : np.ndarray
            Numpy array containing the input features.
        verbose : int
            Parameter to control the verbosity that is given to the predict function of the wrapped model. Thus, the
            specific meaning may depend on the machine learning library used. (Default value = 0)

        Returns
        -------
        np.ndarray
            The numpy array of model predictions.
        """
        if True:  # self.run_config.model_type == "Keras":
            return self.model.predict(inputs, verbose=verbose)

    def loss(self, y_true: np.ndarray, y_pred: np.ndarray, sample_weight: Optional[np.ndarray] = None) -> float:
        """Calculates the loss for the provided arrays of targets and predictions using the loss function attached to the model.

        Parameters
        ----------
        y_true : np.ndarray
            Numpy array of target values.
        y_pred : np.ndarray
            Numpy array of model predictions.
        sample_weight : Optional[np.ndarray]
            Numpy array of sample weights. (Default value = None)

        Returns
        -------
        float
            The loss value.
        """
        if True:  # self.run_config.model_type == "Keras":
            return self.model.loss(
                tf.constant(y_true, dtype=tf.float32),
                tf.constant(y_pred, dtype=tf.float32),
                sample_weight=tf.constant(sample_weight, dtype=tf.float32) if sample_weight is not None else None,
            )


def load_model(run_config: ModuleType, path: str) -> AbstractModel:
    """Helper function that abstracts the model loading for different machine learning libraries.

    Parameters
    ----------
    run_config : ModuleType
        Reference to the imported `run-config`-file.
    path : str
        Path to the model to load. The following file extensions are assumed:
        - ``'.keras'``: Keras model
        - ``'.ckpt'``: Torch / Lightning-model
        If the provided path does not contain a file extension, it will be appended.

    Returns
    -------
    AbstractModel
        The reloaded model wrapped in an ``AbstractModel`` instance.
    """
    if path[-6:] != ".keras":
        path += ".keras"
    return AbstractModel(run_config=run_config, model=tf.keras.models.load_model(path))

    # # the following will be used once Lightning support is implemented:
    # if run_config.model_type == "Keras":
    #     if path[-6:] != ".keras":
    #         path += ".keras"
    #     return AbstractModel(run_config=run_config, model=tf.keras.models.load_model(path))
    # elif run_config.model_type == "Lightning":
    #     if path[-5:] != ".ckpt":
    #         path += ".ckpt"
    #     return AbstractModel(run_config=run_config, model=run_config.LitModel.load_from_checkpoint(path))
    # elif run_config.model_type == "Torch":
    #     if path[-5:] != ".ckpt":
    #         path += ".ckpt"
    #     return AbstractModel(run_config=run_config, model=torch.load(path))


def configure_environment(run_config: ModuleType, cpus: int, gpu_memory: Union[int, float]) -> None:
    """Helper function that abstracts away the configuration of the machine learning library.

    Parameters
    ----------
    run_config : ModuleType
        Reference to the imported `run-config`-file.
    cpus : int
        The number of cpu cores available for this instance.
    gpu_memory : Union[int, float]
        The amount of gpu VRAM in MB available for this instance.
    """
    if True:  # run_config.model_type == "Keras":
        from tensorflow import config as tf_config

        # limit the gpu vram
        if gpu_memory > 0:
            gpus = tf_config.experimental.list_physical_devices("GPU")
            for gpu in gpus:
                tf_config.experimental.set_virtual_device_configuration(
                    gpu, [tf_config.experimental.VirtualDeviceConfiguration(memory_limit=gpu_memory)]
                )

        # reduce number of threads
        try:
            tf_config.threading.set_inter_op_parallelism_threads(min(cpus, 2))
            tf_config.threading.set_intra_op_parallelism_threads(cpus)
        except RuntimeError:
            pass
