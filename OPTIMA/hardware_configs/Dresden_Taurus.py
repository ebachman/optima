from typing import Optional

from .common import SLURMCluster, SLURMClusterJob


class Romeo(SLURMCluster):
    cpus_per_node = 128
    gpus_per_node = 0
    mem_per_cpu = 1972
    threads_per_cpu_core = 2
    SMT_included_in_reservation = False
    vram_per_gpu = 0
    ray_headnodes_path = "/projects/materie-09/s7701085/running_ray_headnodes/running_ray_headnodes.pickle"
    ray_temp_path = "/tmp/ray"  # default

    def submit_job(self, job: "SLURMClusterJob", job_file_path: str, dry_run: bool = False) -> None:
        job.partition = "romeo"
        job.account = "materie-09"
        job.use_SMT = False
        super().submit_job(job, job_file_path, dry_run)

    def _write_job_config(self, job: "SLURMClusterJob") -> str:
        config = "#!/usr/bin/env bash\n\n"
        config += super()._write_job_config(job)
        return config


class Barnard(SLURMCluster):
    cpus_per_node = 104
    gpus_per_node = 0
    mem_per_cpu = 4800
    threads_per_cpu_core = 2
    SMT_included_in_reservation = False
    vram_per_gpu = 0
    ray_headnodes_path = "/projects/materie-09/OPTIMA/running_ray_headnodes/running_ray_headnodes.pickle"
    ray_temp_path = "/dev/shm/ray"

    def submit_job(self, job: "SLURMClusterJob", job_file_path: str, dry_run: bool = False) -> None:
        job.account = "materie-09"
        job.use_SMT = False
        super().submit_job(job, job_file_path, dry_run)

    def _write_job_config(self, job: "SLURMClusterJob") -> str:
        config = "#!/usr/bin/env bash\n\n"
        config += super()._write_job_config(job)
        return config

