# -*- coding: utf-8 -*-
"""A package that provides built-in functionality for classification using multilayer perceptrons with Keras.

All functions defined in this package can be overwritten by redefining them in the `run-config` to configure `OPTIMA` for
other use cases.
"""
