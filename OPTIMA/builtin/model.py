# -*- coding: utf-8 -*-
"""A module that provides functions to prepare and update the built-in Keras multilayer perceptron."""
# This way of providing the model specific functionality is in preparation of supporting different machine learning
# libraries besides Keras, in which case a distinction would be made here.
from OPTIMA.keras.model import build_model, compile_model, update_model  # noqa: F401
