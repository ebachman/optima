#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Convenience wrapper for running OPTIMA directly from source tree."""

from OPTIMA.optima import main

if __name__ == '__main__':
    main()