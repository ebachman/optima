import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from OPTIMA import optima
from OPTIMA.core import evaluation, search_space, inputs, tools, training
from OPTIMA.builtin import inputs as builtin_inputs
from OPTIMA.builtin import search_space as builtin_search_space
from OPTIMA.builtin import model as builtin_model
from OPTIMA.keras import tools as keras_tools
from OPTIMA.keras import training as keras_training