import os
import numpy as np
from OPTIMA.keras import tools
from OPTIMA.builtin import figures_of_merit

# input data and splittings
inputs_file = os.path.join("tests", "resources", "events.pickle")
use_testing_dataset = False
fixed_testing_dataset = True
use_eventNums_splitting = True
eventNums_splitting_offset_val = 0
eventNums_splitting_offset_test = 1
eventNums_splitting_N = 5
validation_fraction = 0.2
test_fraction = 0.2
max_num_events = np.inf
max_event_weight = np.inf

# output folder
archive_path = ""
output_name = "test_optimization"
output_path = "tests"
use_exact_name = True
produce_inputs_plots = False

# which input variables to use for initial hyperopt
input_vars = ['jet1_pt', 'jet1_phi_recalibrated', 'jet1_eta',
              'jet2_pt', 'jet2_phi_recalibrated', 'jet2_eta',
              'lep1_pt', 'lep1_eta',
              'lep2_pt', 'lep2_phi_recalibrated', 'lep2_eta',
              'met_pt', 'met_phi_recalibrated']

# input scaling for each input variable
input_scaling = {
            # low level variables
            "jet1_pt": lambda x: np.log10(x),
            "jet1_eta": lambda x: x,
            "jet1_phi": lambda x: x,
            "jet1_phi_recalibrated": lambda x: x,
            "jet2_pt": lambda x: np.log10(x),
            "jet2_eta": lambda x: x,
            "jet2_phi": lambda x: x,
            "jet2_phi_recalibrated": lambda x: x,
            "lep1_pt": lambda x: np.log10(x),
            "lep1_eta": lambda x: x,
            "lep1_phi": lambda x: x,
            "lep1_phi_recalibrated": lambda x: x,
            "lep2_pt": lambda x: np.log10(x),
            "lep2_eta": lambda x: x,
            "lep2_phi_recalibrated": lambda x: x,
            "met_pt": lambda x: np.log10(x),
            "met_phi": lambda x: x,
            "met_phi_recalibrated": lambda x: x,

            # high level variables
            "jet1_rap": lambda x: x,
            "jet2_rap": lambda x: x,
            "lep1_zep": lambda x: np.sqrt(x),
            "lep2_zep": lambda x: np.sqrt(x),
            "jets_dY": lambda x: x,
            "jets_dPhi": lambda x: x,
            "jets_dR": lambda x: x,
            "leps_dEta": lambda x: np.sqrt(x),
            "leps_dPhi": lambda x: x,
            "leps_dR": lambda x: x,
            "leps_m_ll": lambda x: np.log10(x),
            "leps_sumPt": lambda x: np.sqrt(x),
            "leps_absCosThetaCS": lambda x: x,
            "leps_pt_thrustAxis": lambda x: np.sqrt(x),
            "lepjet_ratioPtProd": lambda x: np.log10(x + 0.02),
            "lepjet_minDR": lambda x: x,
            "leps_m_T": lambda x: np.sqrt(x),
            "leps_m_o1": lambda x: np.sqrt(x),
}

# general settings for the optimization
monitor_name = 'val_bce_loss'
monitor_op = 'min'
optimize_on_best_value = False
restore_on_best_checkpoint = False
max_epochs = 15
early_stopping_patience = 6
overtraining_patience = 6
random_seed = 42

# settings for the evaluation
fit_min_R_squared = 0.9
check_overtraining_with_fit = True
use_early_stopping_for_crossvalidation = False
use_OT_conditions_for_crossvalidation = False
reuse_seed_for_crossvalidation = True
fixed_seed_for_crossvalidation = False
evaluation_class_labels = ["Signal", "Background"]

# settings for the initial optimization phase and the variable optimization
perform_variable_opt = True
num_samples_variableOpt = 4
var_metric = 'loss'
var_metric_op = 'min'
variable_optimization_mode = 'shuffle'
acceptance_criterion = "improvement"
max_rel_change = 0.5
var_opt_patience = 1
choose_best_var_set = True
hybrid_revert_to_best_before_switch = True
num_repeats = 1
num_repeats_hybrid_retrain = 1
reevaluate_candidate_to_drop = True
retrain_for_reevaluation = True
num_repeats_for_reevaluation = 1
use_median_for_averages = False
use_fit_results_for_varOpt = True

# settings for Optuna and the Asynchronous HyperBand scheduler (ASHA)
perform_main_hyperopt = True
use_TPESampler = True
use_ASHAScheduler = True
num_samples_main = 8
ASHA_grace_period = 10
ASHA_max_t = 75
ASHA_reduction_factor = 2
use_multivariate_TPE = False

# PBT specific settings
perform_PBT_hyperopt = True
use_fit_results_for_PBT = True
num_samples_PBT = 4
perturbation_interval = 6
burn_in_period = 6

search_space = {
    "num_layers": ('uniform', 2, 4, 1),
    "units": ('log', 8, 16, 1),
    "activation": ['swish', 'relu'],
    "kernel_initializer": ['glorot_uniform', 'he_normal'],
    "bias_initializer": 'auto',
    "l1_lambda": ('log', 1e-18, 1e-1),
    "l2_lambda": ('log', 1e-18, 1e-1),
    "dropout": ('uniform', 0.0, 0.2),
    "batch_size": ('uniform', 64., 256.),
    "learning_rate": ('log', 1e-5, 1e-2),
    "Adam_beta_1": ('uniform', 1e-4, 0.99),
    "one_minus_Adam_beta_2": ('log', 1e-5, 0.9999),
    "Adam_epsilon": ('log', 1e-10, 1.0),
    "loss_function": 'BinaryCrossentropy',
    "loss_signal_weight": ('uniform', 0.5, 1.5),
}
optimize_units_per_layer = False

custom_metrics = [
    ('bce_loss', tools.WeightedBinaryCrossentropy(only_numpy=True).calc_loss),
    ('LogLikelihoodRatio', figures_of_merit.build_FoM(name='LogLikelihoodRatio', exp_sig=20.002, exp_bkg=394.450, min_events_per_bin=20., N_bins=20)),
]
Keras_metrics = []
weighted_Keras_metrics = []
composite_metrics = []
overtraining_conditions = []
